pynessus
========

Docker_ image to run `pynessus <https://gitlab.esss.lu.se/ics-infrastructure/pynessus>`_, a command line tool for launching Nessus scans NESSUS_.

.. _Docker: https://www.docker.com
.. _NESSUS: https://www.tenable.com/products/nessus

FROM python:3.8-alpine

LABEL maintainer "remy.mudingay@esss.se"

RUN apk add --no-cache libxml2-utils
RUN apk add --no-cache bash git curl
RUN pip install --no-cache-dir argparse==1.4.0
RUN pip install --no-cache-dir requests==2.24.0
RUN pip install --no-cache-dir termcolor==1.1.0

RUN mkdir -p /home/run && \
          cd /home/run && \
          git clone https://gitlab.esss.lu.se/ics-infrastructure/pynessus.git && \
          cd pynessus && \
          git checkout 0.0.1

WORKDIR /home/run/pynessus/bin

# RUN ./pynessus -t $NESSUS_NETWORK -p "$NESSUS_POLICY" -n $NESSUS_SCAN -e $NESSUS_FILE
# RUN curl -F "data=@/home/run/pynessus/$NESSUS_FILE" $NESSUS_UPLOAD
